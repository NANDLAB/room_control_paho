#include "timer_manager.hpp"

typedef timer_manager self;
using boost::asio::steady_timer;
using std::map;

self::timer_manager(boost::asio::io_context &ioc)
    : ioc(ioc), next_timerid(0), timers()
{}

int self::cancel(tid timerid) {
    return timers.erase(timerid);
}

void self::cancel_all() {
    timers.clear();
}
